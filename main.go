package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/apis"
	"github.com/pocketbase/pocketbase/core"
	"github.com/pocketbase/pocketbase/plugins/ghupdate"
	"github.com/pocketbase/pocketbase/plugins/jsvm"
	"github.com/pocketbase/pocketbase/plugins/migratecmd"

	"github.com/stripe/stripe-go/v74"
	"github.com/stripe/stripe-go/v74/customer"
	"github.com/stripe/stripe-go/v74/invoice"
	"github.com/stripe/stripe-go/v74/invoiceitem"
	"github.com/stripe/stripe-go/v74/price"
	"github.com/stripe/stripe-go/v74/product"
)

type MyCustomer struct {
	StripeId string
	Email    string
}

type MyPrice struct {
	StripeId string
	Tier     string
}

type Product struct {
	Custom   bool
	Id       string
	Title    string
	Price    string
	Variant  []string
	Quantity int
}

func main() {
	app := pocketbase.New()

	// ---------------------------------------------------------------
	// Optional plugin flags:
	// ---------------------------------------------------------------

	var migrationsDir string
	app.RootCmd.PersistentFlags().StringVar(
		&migrationsDir,
		"migrationsDir",
		"",
		"the directory with the user defined migrations",
	)

	var automigrate bool
	app.RootCmd.PersistentFlags().BoolVar(
		&automigrate,
		"automigrate",
		true,
		"enable/disable auto migrations",
	)

	var publicDir string
	app.RootCmd.PersistentFlags().StringVar(
		&publicDir,
		"publicDir",
		defaultPublicDir(),
		"the directory to serve static files",
	)

	var indexFallback bool
	app.RootCmd.PersistentFlags().BoolVar(
		&indexFallback,
		"indexFallback",
		true,
		"fallback the request to index.html on missing static path (eg. when pretty urls are used with SPA)",
	)

	var queryTimeout int
	app.RootCmd.PersistentFlags().IntVar(
		&queryTimeout,
		"queryTimeout",
		30,
		"the default SELECT queries timeout in seconds",
	)

	app.RootCmd.ParseFlags(os.Args[1:])

	// ---------------------------------------------------------------
	// Plugins and hooks:
	// ---------------------------------------------------------------

	// load js pb_migrations
	jsvm.MustRegisterMigrations(app, &jsvm.MigrationsOptions{
		Dir: migrationsDir,
	})

	// migrate command (with js templates)
	migratecmd.MustRegister(app, app.RootCmd, &migratecmd.Options{
		TemplateLang: migratecmd.TemplateLangJS,
		Automigrate:  automigrate,
		Dir:          migrationsDir,
	})

	// GitHub selfupdate
	ghupdate.MustRegister(app, app.RootCmd, nil)

	app.OnAfterBootstrap().Add(func(e *core.BootstrapEvent) error {
		app.Dao().ModelQueryTimeout = time.Duration(queryTimeout) * time.Second
		return nil
	})

	app.OnBeforeServe().Add(func(e *core.ServeEvent) error {
		// serves static files from the provided public dir (if exists)
		e.Router.GET("/*", apis.StaticDirectoryHandler(os.DirFS(publicDir), indexFallback))
		return nil
	})

	app.OnRecordBeforeCreateRequest().Add(func(e *core.RecordCreateEvent) error {
		// overwrite the newly submitted "posts" record status to pending
		if e.Record.Collection().Name == "orders" {
			// e.Record.Set("order_status", "cancelled")
			// print("Hello world!")
		}

		return nil
	})

	app.OnRecordAfterCreateRequest().Add(func(e *core.RecordCreateEvent) error {
		if e.Record.Collection().Name == "orders" {
			//log.Println(e.Record.Id)
			// log.Println(e.Record.Get("payment_method").(string))
			customerRecord, _ := app.Dao().FindRecordById("customers", e.Record.GetString("customer"))

			var products []Product
			json.Unmarshal([]byte(e.Record.GetString("products")), &products)
			//fmt.Print("products:", products)
			//fmt.Print("products:", products[0].Title)

			var payment_method []string
			json.Unmarshal([]byte(e.Record.GetString("payment_method")), &payment_method) // string to arr
			fmt.Print("payment_method:", payment_method)
			for _, n := range payment_method {
				if n == "bank_transfer" {
					print("got bank_transfer")
				} else if n == "fpx" {
					print("got fpx")
				} else if n == "stripe" {
					print("got stripe\n")
					print("customer id:", e.Record.GetString("customer")+"\n")
					//print("customer:",record.GetString("name"))
					sendInvoice(customerRecord.GetString("email"), products)
				}
			}
			// if e.Record.payment_method
		}
		return nil
	})

	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}

// the default pb_public dir location is relative to the executable
func defaultPublicDir() string {
	if strings.HasPrefix(os.Args[0], os.TempDir()) {
		// most likely ran with go run
		return "./pb_public"
	}
	return filepath.Join(os.Args[0], "../pb_public")
}

// stripe
func sendInvoice(email string, products []Product) {
	// This is your test secret API key.
	stripe.Key = "sk_test_51NDNJfJTysK5481BygK2tVNHdnuY656bNvZX70gqIm3aEHJqzJu5jx1EathSWp0onfe2st2vUpxBE2DODHlXmsJb00uGvD4YMb"

	productParams := &stripe.ProductParams{Name: stripe.String(products[0].Title)}
	productResult, _ := product.New(productParams)
	print("create product:", productResult.ID, "\n\n")
	// fmt.Printf("create product: %+v\n\n", productResult)

	unitPrice, _ := strconv.ParseInt(products[0].Price, 10, 32)
	priceParams := &stripe.PriceParams{
		Product:    stripe.String(productResult.ID),
		UnitAmount: stripe.Int64(unitPrice * 100),
		Currency:   stripe.String(string(stripe.CurrencyMYR)),
	}
	priceResult, _ := price.New(priceParams)
	print("create price:", priceResult.ID, "\n\n")

	customerID := ""
	if customerID == "" {
		params := &stripe.CustomerParams{
			Email:       stripe.String(email),
			Description: stripe.String("Customer to invoice"),
		}
		cus, _ := customer.New(params)
		// newCus := MyCustomer{
		// 	Email:    email,
		// 	StripeId: cus.ID,
		// }
		// customers = append(customers, newCus)
		customerID = cus.ID
	}

	// Create an Invoice
	inParams := &stripe.InvoiceParams{
		Customer:         stripe.String(customerID),
		CollectionMethod: stripe.String("send_invoice"),
		DaysUntilDue:     stripe.Int64(30),
	}
	in, _ := invoice.New(inParams)
	// Create an Invoice Item with the Price, and Customer you want to charge
	iiParams := &stripe.InvoiceItemParams{
		Customer: stripe.String(customerID),
		Price:    stripe.String(priceResult.ID),
		Invoice:  stripe.String(in.ID),
	}
	invoiceitem.New(iiParams)
	//fmt.Printf("create invoice: %+v\n\n", invoiceResult)
	// Send the Invoice
	params := &stripe.InvoiceSendInvoiceParams{}
	// fmt.Print("invoce:", params)
	invoice.SendInvoice(in.ID, params)
	fmt.Printf("create invoice: %+v\n\n", in)

}
